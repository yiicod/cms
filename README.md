Cms extensions
==============


Insert into your composer
-------------------------
```php
"repositories": [
    {
        "type": "git",
        "url": "https://bitbucket.org/codteam/yiicod-impraviext.git"
    },
    {
        "type": "git",
        "url": "https://bitbucket.org/codteam/yiicod-cms.git"
    },
]
```
Didn't tested with different url rules

Config ( This is all config for extensions ):
---------------------------------------------

Insert into config
------------------

```php
'aliases' => array(
    'vendor' => '...',
),
'components' => array(
        'cms' => array(
            'class' => 'yiicod\cms\Cms',
            'modelMap' => array(
                'Cms' => array(
                    'alias' => 'application\models\CmsModel',
                    'class' => 'application\models\CmsModel',
                    'fieldParentId' => 'parentId',
                    'fieldTitle' => 'title',
                    'fieldAlias' => 'alias',
                    'fieldShortDescription' => null,
                    'fieldContent' => 'content',
                    'fieldIsDefault' => 'isDefault',
                    'fieldStatus' => 'status',
                    'fieldCreateDate' => 'createDate',
                    'fieldUpdateDate' => 'updateDate',
                )
            ),
            'default' => array(
                'cms' => array(
                    'layout' => '',
                    'filters' => array(),
                    'accessRules' => array(),
                ),
            ),
            'admin' => array(
                'cms' => array(
                    'layout' => '/layouts/column2',
                    'filters' => array(),
                    'accessRules' => array(),
                )
            ),
        ),
        'components' => array(),
    ),
)
...
'preload' => array('cms')
```

Using
-----