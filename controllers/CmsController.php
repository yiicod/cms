<?php

namespace yiicod\cms\controllers;

use CController;
use Yii;
use CMap;

/**
 * 
 */
class CmsController extends CController
{

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        $this->layout = Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['layout'];
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::filters(), Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['filters']
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            'view' => [
                'class' => 'yiicod\cms\actions\cms\ViewAction',
            ],
        ];
    }

}
