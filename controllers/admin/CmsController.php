<?php

namespace yiicod\cms\controllers\admin;

use Controller;
use Yii;
use CMap;

/**
 * 
 */
class CmsController extends Controller
{

    public $defaultAction = 'admin';

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        if (Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['layout']) {
            $this->layout = Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['layout'];
        }
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::filters(), Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['filters']
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('cms')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            'admin' => [
                'class' => 'yiicod\cms\actions\admin\cms\AdminAction',
            ],
            'update' => [
                'class' => 'yiicod\cms\actions\admin\cms\UpdateAction',
            ],
            'create' => [
                'class' => 'yiicod\cms\actions\admin\cms\CreateAction',
            ],
            'delete' => [
                'class' => 'yiicod\cms\actions\admin\cms\DeleteAction',
            ],
            'imageUpload' => [
                'class' => 'yiicod\impraviext\actions\ImageUpload',
                'uploadPath' => Yii::getPathOfAlias('webroot') . '/uploads',
                'uploadUrl' => Yii::app()->getBaseUrl(true) . '/uploads',
                'params' => ['id'],
                'uploadCreate' => true,
            ],
        ];
    }

}
