<?php

namespace yiicod\cms\models\behaviors;

use CActiveRecordBehavior;
use CDbCriteria;
use CHtml;
use CMap;
use Yii;

class CmsExtBehavior extends CActiveRecordBehavior
{

    /**
     * Generate nesting parent folder tree from up to down
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @return array
     */
    public static function generateNestingTree($id)
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];
        $items = $modelCms::model()->findAll(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'] . '=:parentId', ['parentId' => $id]);

        $dataTree = [$id];

        if (!count($items)) {
            return $dataTree;
        }

        foreach ($items as $item) {
            if (!is_null($modelCms::model()->find(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'] . '= :parentId', ['parentId' => $item->id]))) {
                $dataTree = CMap::mergeArray($dataTree, self::generateNestingTree($item->id, 0));
            } else {
                $dataTree[] = $item->id;
            }
        }
        sort($dataTree);
        return array_unique($dataTree);
    }

    /**
     * Generate alias auto
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>     
     * @return boolean
     */
    public function generateAlias($i = 0)
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];
        $fieldAlias = Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'];
        $alias = $this->getOwner()->getAlias();
        if (empty($alias)) {
            $this->getOwner()->setAlias(preg_replace('/[^A-Za-z0-9-]+/', '-', mb_strtolower(trim($this->getOwner()->getTitle()))));
        }

        $model = $modelCms::model()->find($fieldAlias . ' = :alias AND id<>:id', [':alias' => $this->getOwner()->getAlias(), ':id' => is_null($this->getOwner()->id) ? 0 : $this->getOwner()->id]);

        if (null !== $model) {
            $this->getOwner()->setAlias(preg_replace('/[^A-Za-z0-9-]+/', '-', mb_strtolower(trim($this->getOwner()->getTitle())) . '-' . ++$i));
            return $this->generateAlias($i);
        }
        return true;
    }

    /**
     * Get valid parents list
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * @return Array
     */
    public function getValidParents()
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];

        $criteria = new CDbCriteria();
        $fieldTitle = Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'];
        if ($this->getOwner()->id) {
            $criteria->addNotInCondition('id', $this->generateNestingTree($this->getOwner()->id));
            $criteria->addCondition('id<>:id');
            $criteria->params = CMap::mergeArray($criteria->params, ['id' => $this->getOwner()->id]);
        }
        return CHtml::listData($modelCms::model()->findAll($criteria), 'id', $fieldTitle);
    }

    /**
     * Get dafault view url
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * @return string
     */
    public function getDefaultViewUrl($args, $adminRules = false)
    {
        return Yii::app()->createAbsoluteUrl(Yii::app()->getComponent("cms")->defaultViewRoute, $args);
    }

    public function getLayoutListData($index = null)
    {
        $items = @call_user_func(Yii::app()->getComponent('cms')->modelMap['Cms']['layout']);
        if (null === $items) {
            $items = Yii::app()->getComponent('cms')->modelMap['Cms']['layout'];
        }
        if (!is_null($index)) {
            return $items[$index];
        }
        return $items;
    }

    public function canUseDefaultViewRoute()
    {
        return Yii::app()->getComponent("cms")->defaultViewRoute !== false && $this->getOwner()->isNewRecord === false;
    }

    public function canUseLayout()
    {
        if (count($this->getLayoutListData())) {
            return true;
        }
        return false;
    }

    public function beforeSave($event)
    {
        parent::beforeSave($event);

        $this->generateAlias();
        //Fix quotes
        $this->getOwner()->setContent(stripslashes($this->getOwner()->getContent()));
        $this->getOwner()->setShortDescription(stripslashes($this->getOwner()->getShortDescription()));
    }

}
