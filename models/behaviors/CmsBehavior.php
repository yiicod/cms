<?php
namespace yiicod\cms\models\behaviors;

use CActiveRecordBehavior;
use Yii;

/*
  'fieldCreateDate' => 'dateCreate',
  'fieldUpdateDate' => 'UpdateDate',
 */

class CmsBehavior extends CActiveRecordBehavior
{

    /**
     * Set parent id
     * @param int $value
     */
    public function setParentId($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId']} = $value;
        }
    }

    /**
     * Set title
     * @param string $value
     */
    public function setTitle($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle']} = $value;
        }
    }

    /**
     * Set alias
     * @param string $value
     */
    public function setAlias($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias']} = mb_strtolower($value);
        }
    }

    /**
     * Set shortDescription
     * @param string $value
     */
    public function setShortDescription($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription']} = $value;
        }
    }

    /**
     * Set content
     * @param string $value
     */
    public function setContent($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent']} = $value;
        }
    }

    /**
     * Set isDefault
     * @param int $value
     */
    public function setIsDefault($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldIsDefault'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldIsDefault']} = $value;
        }
    }

    /**
     * Set status
     * @param int $value
     */
    public function setStatus($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus']} = $value;
        }
    }

    /**
     * Set status
     * @param int $value
     */
    public function setLayout($value)
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout']} = $value;
        }
    }

    /**
     * Get parent id
     * @return int 
     */
    public function getParentId()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId']};
        }
        return 0;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle']};
        }
        return '';
    }

    /**
     * Get title
     * @return string
     */
    public function getAlias()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias']};
        }
        return '';
    }

    /**
     * Get Short Description
     * @return string
     */
    public function getShortDescription()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription']};
        }
        return '';
    }

    /**
     * Get Content
     * @return string
     */
    public function getContent()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent']};
        }
        return '';
    }

    /**
     * Get is default
     * @return string
     */
    public function getIsDefault()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldIsDefault'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldIsDefault']};
        }
        return 0;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus']};
        }
        return 0;
    }

    /**
     * Set status
     * @param int $value
     */
    public function getLayout()
    {
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout']};
        }
        return 0;
    }

}
