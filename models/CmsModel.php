<?php

namespace yiicod\cms\models;

use CDbCriteria;
use CActiveDataProvider;
use CActiveRecord;
use CMap;
use Yii;

/**
 * This is the model class for table "ECms".
 *
 * The followings are the available columns in table 'ECms':
 * @property integer $id
 * @property integer $parentId
 * @property string $title
 * @property string $content
 * @property string $layout
 * @property string $alias
 * @property string $dateCreate
 * @property string $dateUpdate
 * @property string $metaTitle
 * @property string $metaKeyword
 * @property string $metaDescription
 * @property integer $isDefault
 */
class CmsModel extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Cms';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title', 'required'],
            //array('alias', 'unique'),
            ['parentId, isDefault', 'numerical', 'integerOnly' => true],
            ['title, alias', 'length', 'max' => 100],
            ['content, createDate, updateDate, parentId, shortDescription, content, layout', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, parentId, title, content, alias, dateCreate, dateUpdate, isDefault', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'parent' => [self::HAS_ONE, 'application\models\CmsModel', ['id' => 'parentId']],
            'childs' => [self::HAS_MANY, 'application\models\CmsModel', 'parentId'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cms', 'ID'),
            'parentId' => Yii::t('cms', 'Parent'),
            'title' => Yii::t('cms', 'Title'),
            'content' => Yii::t('cms', 'Content'),
            'alias' => Yii::t('cms', 'Alias'),
            'dateCreate' => Yii::t('cms', 'Date Create'),
            'dateUpdate' => Yii::t('cms', 'Date Update'),
            'isDefault' => Yii::t('cms', 'Is Default'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $this->attributeNames())) {
            $criteria->compare(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $this->parentId);
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('alias', $this->alias, true);
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldCreateDate'], $this->attributeNames())) {
            $criteria->compare(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldCreateDate'], $this->createDate, true);
        }
        if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldUpdateDate'], $this->attributeNames())) {
            $criteria->compare(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldUpdateDate'], $this->updateDate, true);
        }

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmsModel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        if (file_exists(Yii::getPathOfAlias('application.models.behaviors.XssBehavior') . '.php')) {
            $behaviors['XssBehavior'] = [
                'class' => 'application.models.behaviors.XssBehavior',                
            ];
        }

        $behaviors['CmsBehavior'] = [
            'class' => 'yiicod\cms\models\behaviors\CmsBehavior',
        ];
        $behaviors['ExtCmsBehavior'] = [
            'class' => 'yiicod\cms\models\behaviors\CmsExtBehavior',
        ];
        $behaviors['CTimestampBehavior'] = [
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldCreateDate'], $this->attributeNames()) ?
                    Yii::app()->getComponent('cms')->modelMap['Cms']['fieldCreateDate'] : null,
            'updateAttribute' => in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldUpdateDate'], $this->attributeNames()) ?
                    Yii::app()->getComponent('cms')->modelMap['Cms']['fieldUpdateDate'] : null,
        ];

        return CMap::mergeArray(parent::behaviors(), $behaviors);
    }

}
