<?php

namespace yiicod\cms\actions\cms;

use yiicod\cms\actions\BaseAction;
use Yii;
use CHttpException;
use CDbCriteria;

/**
 * Update a model.
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If update is successful, the browser will be redirected to the 'admin' page.
 */
class ViewAction extends BaseAction
{

    public $view = 'yiicod.cms.views.cms.view';
    public $criteria = null;

    public function run($alias)
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];

        $criteria = new CDbCriteria();
        $criteria->condition = Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'] . '=:alias';
        $criteria->params = ['alias' => $alias];
        if (!$this->criteria instanceof CDbCriteria && is_array($this->criteria)) {
            $this->criteria = new CDbCriteria($this->criteria);
            $criteria->mergeWith($this->criteria);
        } elseif ($this->criteria instanceof CDbCriteria) {
            $criteria->mergeWith($this->criteria);
        }

        $model = $modelCms::model()->find($criteria);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        if ($model->canUseLayout()) {
            $this->getController()->layout = $model->getLayout();
        }

        $this->getController()->render($this->view, [
            'model' => $model,
        ]);
    }

}
