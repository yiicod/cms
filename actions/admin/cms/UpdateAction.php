<?php

namespace yiicod\cms\actions\admin\cms;

use CHtml;
use CHttpException;
use Yii;
use yiicod\cms\actions\BaseAction;

/**
 * Update a model.
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If update is successful, the browser will be redirected to the 'admin' page.
 */
class UpdateAction extends BaseAction
{

    public $view = 'yiicod.cms.views.admin.cms.update';

    public function run($id)
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];

        $model = $modelCms::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST[CHtml::modelName($model)])) {
            $model->attributes = $_POST[CHtml::modelName($model)];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('cms', 'Record update success'));
                Yii::app()->controller->redirect(['update', 'id' => $model->id]);
            }
        }

        Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}
