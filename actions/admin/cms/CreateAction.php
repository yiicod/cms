<?php

namespace yiicod\cms\actions\admin\cms;

use CHtml;
use Yii;
use yiicod\cms\actions\BaseAction;

/**
 * Creates a new model.
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If creation is successful, the browser will be redirected to the 'admin' page.
 */
class CreateAction extends BaseAction
{

    public $view = 'yiicod.cms.views.admin.cms.create';

    public function run()
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];

        $model = new $modelCms;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST[CHtml::modelName($model)])) {
            $model->attributes = $_POST[CHtml::modelName($model)];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('cms', 'Record create success'));
                Yii::app()->controller->redirect(['update', 'id' => $model->id]);
            }
        }

        Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}
