<?php

namespace yiicod\cms\actions\admin\cms;

use CHttpException;
use Yii;
use yiicod\cms\actions\BaseAction;

/**
 * Delate a model.
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If delete is successful, the browser will be redirected to the 'admin' page.
 */
class DeleteAction extends BaseAction
{

    public function run($id)
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];
        $model = $modelCms::model()->findByPk($id);
        if ($model->getIsDefault()) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            Yii::app()->controller->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['admin']);
        }
    }

}