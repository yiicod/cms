<?php

namespace yiicod\cms\actions\admin\cms;

use CHtml;
use Yii;
use yiicod\cms\actions\BaseAction;

/**
 * Creates a new model.
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * If creation is successful, the browser will be redirected to the 'admin' page.
 */
class AdminAction extends BaseAction
{

    public $view = 'yiicod.cms.views.admin.cms.admin';

    public function run()
    {
        $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];

        $model = new $modelCms;

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET[CHtml::modelName($model)]))
            $model->attributes = $_GET[CHtml::modelName($model)];

        Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}