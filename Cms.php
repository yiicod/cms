<?php

namespace yiicod\cms;

use CMap;
use Yii;
use CApplicationComponent;

/**
 * Cms extension settings
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class Cms extends CApplicationComponent
{

    /**
     * @var ARRAY table settings
     */
    public $modelMap = [];

    /**
     * @var ARRAY components settings
     */
    public $components = [];

    /**
     * @var array Controllers settings
     */
    public $controllers = [];

    /**
     * Default route
     */
    public $defaultViewRoute = null;

    /**
     * Layouts list
     * In new versions deprecated
     */
    public $layouts = [];

    public function init()
    {
        parent::init();
        //Merge main extension config with local extension config
        $config = include(dirname(__FILE__) . '/config/main.php');
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $this->{$key} = CMap::mergeArray($value, $this->{$key});
            } elseif (null === $this->{$key}) {
                $this->{$key} = $value;
            }
        }

        //For old versions begin
        $this->modelMap['Cms']['layout'] = CMap::mergeArray($this->modelMap['Cms']['layout'], $this->layouts);
        //For old versions end

        if (!Yii::app() instanceof \CConsoleApplication) {
            //Merge controllers map
            $route = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $module = substr($route, 0, strpos($route, '/'));

            if (Yii::app()->hasModule($module) && isset($this->controllers['controllerMap'][$module])) {
                Yii::app()->getModule($module)->controllerMap = CMap::mergeArray($this->controllers['controllerMap'][$module], Yii::app()->getModule($module)->controllerMap);
            } elseif (isset($this->controllers['controllerMap']['default'])) {
                Yii::app()->controllerMap = CMap::mergeArray($this->controllers['controllerMap']['default'], Yii::app()->controllerMap);
            }
        }

        Yii::import($this->modelMap['Cms']['alias']);
        Yii::setPathOfAlias('yiicod', realpath(dirname(__FILE__) . '/..'));
        
        //Set components
        if (count($this->components)) {
            $exists = Yii::app()->getComponents(false);
            foreach ($this->components as $component => $params) {
                if (isset($exists[$component]) && is_object($exists[$component])) {
                    unset($this->components[$component]);
                } elseif (isset($exists[$component])) {
                    $this->components[$component] = \CMap::mergeArray($params, $exists[$component]);
                }
            }
            Yii::app()->setComponents(
                $this->components, false
            );
        }
    }

}
