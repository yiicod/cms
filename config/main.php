<?php

return [
    'modelMap' => [
        'Cms' => [
            'alias' => 'yiicod\cms\models\CmsModel',
            'class' => 'yiicod\cms\models\CmsModel',
            'fieldParentId' => 'parentId',
            'fieldTitle' => 'title',
            'fieldAlias' => 'alias',
            'fieldShortDescription' => 'shortDescription',
            'fieldContent' => 'content',
            'fieldLayout' => 'layout',
            'fieldIsDefault' => 'isDefault',
            //'fieldStatus' => 'status', //@todo Maybe delete...
            'fieldCreateDate' => 'createDate',
            'fieldUpdateDate' => 'updateDate',
            //enumerables or array with params
            'layout' => [],
        ]
    ],
    'defaultViewRoute' => false,
    'controllers' => [
        'controllerMap' => [
            'default' => [
                'cms' => 'yiicod\cms\controllers\CmsController',
            ],
            'admin' => [
                'cms' => 'yiicod\cms\controllers\admin\CmsController',
            ]
        ],
        'default' => [
            'cms' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ],
        ],
        'admin' => [
            'cms' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ]
        ],
    ],
    'components' => [],
];

