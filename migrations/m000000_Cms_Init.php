<?php

class m000000_Cms_Init extends CDbMigration
{

    public function up()
    {
        /**
         * If you need short description add 
         * `shortDescription` varchar(100) NOT NULL,
         * If you need parentId ( page tree ) add
         * `parentId` int(11) NOT NULL DEFAULT '0',
         */
        $this->execute("
            CREATE TABLE `Cms` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `alias` varchar(45) NOT NULL,
              `title` varchar(100) NOT NULL,
              `content` text,
              `layout` varchar(45) DEFAULT 'column2',
              `isDefault` int(11) NOT NULL DEFAULT '0',
              `createDate` datetime DEFAULT NULL,
              `updateDate` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("
            DROP TABLE `Cms`;
        ");
    }

}
