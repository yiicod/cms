<?php
/* @var $this BannerController */
/* @var $model BannerModel */
$this->breadcrumbs = [
    Yii::t('cms', 'Manage Cms') => ['admin'],
    Yii::t('cms', 'Update'),
];

$this->menu = [
    ['label' => Yii::t('cms', 'Manage Cms'), 'url' => ['admin']],
    ['label' => Yii::t('cms', 'Create'), 'url' => ['create']],
];
?>

<h1><?php echo Yii::t('cms', 'Update Page "{title}"', ['{title}' => $model->{Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle']}]); ?>"</h1>

<?php $this->renderPartial('yiicod.cms.views.admin.cms._form', ['model' => $model]); ?>