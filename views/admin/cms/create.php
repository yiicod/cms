<?php
/* @var $this CmsController */
/* @var $model CmsModel */

$this->breadcrumbs = [
    Yii::t('cms', 'Manage Cms') => ['admin'],
    Yii::t('cms', 'Create'),
];

$this->menu = [
    ['label' => Yii::t('cms', 'Manage Cms'), 'url' => ['admin']],
];
?>

<h1><?php echo Yii::t('cms', 'Create Page') ?></h1>

<?php $this->renderPartial('yiicod.cms.views.admin.cms._form', ['model' => $model]); ?>