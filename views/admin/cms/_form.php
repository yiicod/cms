<?php
/* @var $this BannerController */
/* @var $model BannerModel */
/* @var $form CActiveForm */
?>

<div class="form form-box">

    <?php
    $modelCms = Yii::app()->getComponent('cms')->modelMap['Cms']['class'];
    $form = $this->beginWidget('CActiveForm', [
        'id' => 'cms-model-form',
//        'type' => 'horizontal',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'htmlOptions' => ['class' => 'well'], // for inset effect
        'clientOptions' => [
            'validateOnChange' => false,
            'validateOnSubmit' => true,
        ],
    ]);
    ?>

    <p class="note"><?php echo Yii::t('cms', 'Fields with {tag} are required.', ['{tag}' => '<span class="required">*</span>']) ?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php if ($model->canUseDefaultViewRoute()): ?>
        <div class="row">
            <div class="control-group ">
                <label class="control-label preview"><?php echo CHtml::link('Preview', $model->getDefaultViewUrl(["alias" => $model->getAlias()]), ['target' => '_blank']) ?></label>
            </div> 
        </div>    
    <?php endif; ?>

    <?php if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'], ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php echo $form->textField($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle']); ?>
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle']); ?>
                </div>
            </div> 
        </div>
    <?php endif; ?>
    <?php if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php echo $form->dropDownList($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId'], $model->getValidParents(), ['prompt' => Yii::t('cms', 'Choose')]); ?>
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldParentId']); ?>
                </div>
            </div>  
        </div>
    <?php endif; ?>    
    <?php if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'], ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php echo $form->textField($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias']); ?>
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias']); ?>
                </div>
            </div>  
        </div>
    <?php endif; ?>
    <?php if ($model->canUseLayout()): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'layout', ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php echo $form->dropDownList($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout'], $model->getLayoutListData()); ?>
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldLayout']); ?>
                </div>
            </div>  
        </div>
    <?php endif; ?>            
    <?php if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'], ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php
                    $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', [
                        'model' => $model,
                        'attribute' => Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'],
                        'options' => [
                            'imageUpload' => Yii::app()->createAbsoluteUrl('/admin/cms/imageUpload', [
                                'attr' => Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription'],
                                    ]
                            ),
                            'removeWithoutAttr' => false,
                            'minHeight' => '200',
                            'replaceDivs' => false,
                        ],
                    ]);
                    ?>               
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldShortDescription']); ?>   
                </div>
            </div>  
        </div>
    <?php endif; ?>
    <?php if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'], ['class' => 'control-label']); ?>
                <div class="controls">
                    <?php
                    $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', [
                        'model' => $model,
                        'attribute' => Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'],
                        'options' => [
                            'imageUpload' => Yii::app()->createAbsoluteUrl('/admin/cms/imageUpload', [
                                'attr' => Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent'],
                                    ]
                            ),
                            'removeWithoutAttr' => false,
                            'minHeight' => '200',
//                            'rangy' => false,
                            'replaceDivs' => false,
                        ],
                    ]);
                    ?>               
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldContent']); ?> 
                </div>
            </div>  
        </div>
    <?php endif; ?>
    <?php /*if (in_array(Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus'], $model->attributeNames())): ?>
        <div class="row">
            <div class="control-group ">
                <?php echo $form->labelEx($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus'], array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->dropDownList($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus'], arraty('')); ?>
                    <?php echo $form->error($model, Yii::app()->getComponent('cms')->modelMap['Cms']['fieldStatus']); ?>
                </div>
            </div>  
        </div>
    <?php endif;*/ ?>

    <div class="form-actions full-buttons clearfix">
        <?php
        echo CHtml::submitButton($model->isNewRecord ? Yii::t('cms', 'Create') : Yii::t('cms', 'Save'));
        ?>
        <?php
        echo CHtml::button(Yii::t('cms', 'Cancel'), ['class' => 'cancel', 'onclick' => 'window.location="' . Yii::app()->request->urlReferrer . '"']);
        ?>        
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->