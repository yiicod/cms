<?php
$this->breadcrumbs = [
    Yii::t('cms', 'Manage Cms'),
];

$this->menu = [
    ['label' => Yii::t('cms', 'Create'), 'url' => ['create']],
];
?>

<h1><?php echo Yii::t('cms', 'Manage Cms') ?></h1>

<?php
$this->widget('zii.widgets.grid.CGridView', [
    'id' => 'cms-model-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'items table table-striped table-bordered',
    'filter' => $model,
    'columns' => [
        Yii::app()->getComponent('cms')->modelMap['Cms']['fieldTitle'],
        Yii::app()->getComponent('cms')->modelMap['Cms']['fieldAlias'],
        [
            'class' => 'CButtonColumn',
            'header' => Yii::t('cms', 'Actions'),
            'template' => '{update}{view}{delete}',
            'buttons' => [
                'delete' => [
                    'visible' => '$data->getIsDefault()==0',
                ],
                'view' => [
                    'url' => '$data->getDefaultViewUrl(array("alias" => $data->getAlias()))',
                    'visible' => 'Yii::app()->getComponent("cms")->defaultViewRoute !== false',
                ],
            ]
        ],
    ],
]);
?>
